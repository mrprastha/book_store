@extends('layouts.master')
@section('title')
Sale
@endsection
@section('content')
<div class="container">
    @if (session()->has('msg'))
        <div class="alert alert-info" role="alert">
            {{session('msg')}}
          </div>

    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">SALE</h3>
      </div>
      <!-- /.card-header -->


      <!-- form start -->
      <form action="{{route('sale.store')}}" method="post">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="title">Book</label>
            <select name="book_id" class="form-control">
                <option value="" selected disabled>Select Books</option>
                @foreach($book as $books)
                <option value="{{$books->id}}">{{$books->title.'--'.$books->author->full_name}}</option>
                @endforeach
            {{-- <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title" value="{{old('title')}}"> --}}
            </select>
          </div>
          <div class="form-group">
            <label for="price">Price</label>
            <input type="number" name="price" class="form-control" id="price" placeholder="Enter Price" value="{{old('price')}}">
          </div>
          <div class="form-group">
            <label for="customer_name">Customer Name</label>
            <input type="text" name="customer_name" class="form-control" id="customer_name" placeholder="Customer Name" value="{{old('customer_name')}}">
          </div>
          <div class="form-group">
            <label for="customer_contact_no">Contact No</label>
            <input type="tel" name="customer_contact_no" class="form-control" id="customer_contact_no" placeholder="012-345-6789"  value="{{old('customer_contact_no')}}">
          </div>
          <div class="form-group">
            <label for="sales_date">Date</label>
            <input type="date" name="sales_date" class="form-control" id="sales_date" value=@php echo date('Y-m-d') @endphp>
          </div>

        </div>


        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="{{route('sale.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
