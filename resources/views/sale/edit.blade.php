@extends('layouts.master')
@section('title')
Sale
@endsection
@section('content')
<div class="container">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">EDIT SALE</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="{{route('sale.update',$sale->id)}}" method="post">
        @csrf @method('put')
        <div class="card-body">
          <div class="form-group">
            <label for="title">Book Title</label>
           <select name="book_id" class="form-control">
               @foreach($book as $book)
               <option value="{{$book->id}}" {{$book->id == $sale->book_id? 'Selected':''}}>{{$book->title.'--'.$book->author->full_name}}</option>
               @endforeach
           </select>
          </div>
          <div class="form-group">
            <label for="price">Price</label>
            <input type="number" name="price" class="form-control" id="price"  value="{{$sale->price}}">
          </div>
          <div class="form-group">
            <label for="customer_name">Customer Name</label>
            <input type="text" name="customer_name" class="form-control" id="customer_name"  value="{{$sale->customer_name}}">
          </div>
          <div class="form-group">
            <label for="customer_contact_no">Contact No</label>
            <input type="tel" name="customer_contact_no" class="form-control" id="customer_contact_no"  value="{{$sale->customer_contact_no}}">
          </div>
          <div class="form-group">
            <label for="sales_date">Sale Date</label>
            <input type="date" name="sales_date" class="form-control" id="sales_date" value="{{$sale->sales_date}}">
          </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <a href="{{route('sale.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
