@extends('layouts.master')
@section('title')
Genre
@endsection
@section('content')
<div class="container">


    @if(Session::has('msg'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('msg') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="text-right">
        <a href="{{route('genre.create')}}" class="btn btn-primary">Add Genre</a>
    </div>


    <div class="card">
      <div class="card-header">
        <h3 class="text-center">List of Genres</h3>

        <div class="card-tools">
          {{-- <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div> --}}
        </div>
      </div>


      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap" id="myTable">
          <thead>
            <tr>
              <th>ID</th>

              <th>Genre</th>

              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach($genre as $genre)
            <tr>
                <input type="hidden" id="delete_id" name="delete_id" value="{{$genre->id}}">
              <td>{{$loop->index+1}}</td>

              <td>{{$genre->name}}</td>

              <td>
                  <form action="{{route('genre.destroy',$genre->id)}}" method="post" >
                    @csrf @method('delete')

                    <a href="{{route('genre.edit',$genre->id)}}" class="btn btn-warning"><i class="fa fa-pencil-alt"></i></a>
                    <button type="submit" id="delete-btn" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                  </form>


              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->

</div>
    <!-- /.card -->
  </div>

  <script>
      $(function(){

          $('#myTable').DataTable();
           $("#delete-btn").on("click",function(e){
            e.preventDefault();
            var delete_id = $("#delete_id").val();

            Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var data = {
                                "_token":$('meta[name="csrf-token"]').attr('content'),
                                'id':delete_id
                            }
                           $.ajax({
                               type:"DELETE",
                               url:"/genre/" + delete_id,
                               data:data,
                               success:function(response){
                                  location.reload();
                                   Swal.fire(
                                         'Deleted!',
                                         'Your file has been deleted.',
                                        'success'
                                   )
                               }
                           })

                        }
                    });

          });
      });

  </script>
   {{-- @if (Session::has('msg'))
  <script>
      toastr.success("{{!!Session::get('msg')!!}}");
  </script>
   @endif --}}
@endsection
