@extends('layouts.master')
@section('title')
Genre
@endsection
@section('content')
<div class="container">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">EDIT GENRE</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="{{route('genre.update',$genre->id)}}" method="post">
        @csrf @method('put')
        <div class="card-body">

          <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" name="name" class="form-control" id="genre" placeholder="Enter Book Author" value="{{$genre->name}}">
          </div>
          {{-- <div class="form-group">
            <label for="published_date">Published Date</label>
            <input type="date" name="published_date" class="form-control" id="published_date" value="{{$books->published_date}}">
          </div> --}}

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <a href="{{route('genre.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
