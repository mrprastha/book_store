@extends('layouts.master')
@section('title')
Genre
@endsection
@section('content')
<div class="container">
    @if (session()->has('msg'))
        <div class="alert alert-info" role="alert">
            {{session('msg')}}
          </div>

    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">ADD GENRES</h3>
      </div>
      <!-- /.card-header -->


      <!-- form start -->
      <form action="{{route('genre.store')}}" method="post">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="genre">Genre</label>
            <input type="text" name="name" class="form-control" id="genre" placeholder="Enter Genre" value="{{old('name')}}">
          </div>


        </div>


        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="{{route('genre.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
