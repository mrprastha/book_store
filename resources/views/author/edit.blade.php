@extends('layouts.master')
@section('title')
Author
@endsection
@section('content')
<div class="container">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif <!-- general form elements -->
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">EDIT Author</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="{{route('author.update',$author->id)}}" method="post">
        @csrf @method('put')
        <div class="card-body">

          <div class="form-group">
            <label for="author">Author Name</label>
            <input type="text" name="full_name" class="form-control" id="author" placeholder="Enter Book Author" value="{{$author->full_name}}">
          </div>
          {{-- <div class="form-group">
            <label for="published_date">Published Date</label>
            <input type="date" name="published_date" class="form-control" id="published_date" value="{{$books->published_date}}">
          </div> --}}

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Update</button>
          <a href="{{route('author.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
