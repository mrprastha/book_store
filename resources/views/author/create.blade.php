@extends('layouts.master')
@section('title')
Author
@endsection
@section('content')
<div class="container">
    @if (session()->has('msg'))
        <div class="alert alert-info" role="alert">
            {{session('msg')}}
          </div>

    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">ADD BOOKS</h3>
      </div>
      <!-- /.card-header -->


      <!-- form start -->
      <form action="{{route('author.store')}}" method="post">
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="title">Author Name</label>
            <input type="text" name="full_name" class="form-control" id="title" placeholder="Enter Full Name" value="{{old('full_name')}}">
          </div>


        </div>


        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="{{route('author.index')}}" class="btn btn-danger">Cancel</a>
        </div>
      </form>
    </div>
    <!-- /.card -->





  </div>
@endsection
