@extends('layouts.master')
    {{-- {{$books}} --}}

{{-- <div class="row py-5">
    <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img src="{{asset('uploads/'.$books->image)}}"  width="300px" height="300px" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">{{$books->title}}</h5>
              <p class="card-text">{{$books->author->full_name}} Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni delectus exercitationem mollitia dolore corporis totam repellendus quae atque facilis impedit!</p>
              <a href="#" class="btn btn-primary">View</a>
            </div>
          </div>
    </div>
</div> --}}



<div class="row">
    <div class="col-md-12 col-lg-6 col-xl-4">
      <div class="card mb-2 bg-gradient-dark">
        <img class="card-img-top" src="" alt="Dist Photo 1">
        <div class="card-img-overlay d-flex flex-column justify-content-end">
          <h5 class="card-title text-primary text-white">Card Title</h5>
          <p class="card-text text-white pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p>
          <a href="#" class="text-white">Last update 2 mins ago</a>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-4">
      <div class="card mb-2">
        <img class="card-img-top" src="" alt="Dist Photo 2">
        <div class="card-img-overlay d-flex flex-column justify-content-center">
          <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
          <p class="card-text pb-2 pt-1 text-white">
            Lorem ipsum dolor sit amet, <br>
            consectetur adipisicing elit <br>
            sed do eiusmod tempor.
          </p>
          <a href="#" class="text-white">Last update 15 hours ago</a>
        </div>
      </div>
    </div>
    <div class="col-md-12 col-lg-6 col-xl-4">
      <div class="card mb-2">
        <img class="card-img-top" src="" alt="Dist Photo 3">
        <div class="card-img-overlay">
          <h5 class="card-title text-primary">Card Title</h5>
          <p class="card-text pb-1 pt-1 text-white">
            Lorem ipsum dolor <br>
            sit amet, consectetur <br>
            adipisicing elit sed <br>
            do eiusmod tempor. </p>
          <a href="#" class="text-primary">Last update 3 days ago</a>
        </div>
      </div>
    </div>
  </div>





