@extends('layouts.front')
@section('content')
<div class="page-title">
    <h1 class="text-center">Book Store</h1>
</div>
<div class="row">
<div class="col-md-8">
    <h1>Title:{{$book->title}}</h1>
    <h1>Author:{{$book->author->full_name}}</h1>
    <h1>Category:{{$book->genre->name}}</h1>
    <h1>Summary:{{$book->summary}}</h1>

</div>
<div class="col-md-4">
    <image src="{{asset('uploads/'.$book->image)}}" height="400px" width="300px"></image>

</div>
</div>

@endsection
