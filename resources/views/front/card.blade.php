@extends('layouts.front')
@section('content')
<div class="page-title">
    <h1 class="text-center">Book Store</h1>
</div>
<div class="row">

@foreach ($books as $books)
<div class="col-md-12 col-lg-6 col-xl-4">
  <div class="card mb-2 bg-gradient-dark">
                <img class="card-img-top" src="{{ asset('uploads/' . $books->image) }}" width="200px" height="200px" alt="Dist Photo 1">
                <div class="card-img-overlay d-flex flex-column justify-content-end">
                    <h5 class="card-title text-primary text-white">{{ $books->title }}</h5>
                    <p class="card-text text-white pb-2 pt-1">{{ $books->author->full_name }}</p>
                    <a href="{{route('frontbook.show',$books->id)}}" class="btn btn-primary text-center">View</a>
                </div>
            </div>
        </div>

@endforeach

</div>

@endsection

