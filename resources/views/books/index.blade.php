@extends('layouts.master')
@section('title')
Sale
@endsection
@section('content')
<div class="container">


    @if(Session::has('msg'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('msg') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <div class="text-right">
        <a href="{{route('book.create')}}" class="btn btn-primary">Add Books</a>
    </div>


    <div class="card">
      <div class="card-header">
        <h3 class="text-center">List of Books</h3>

        <div class="card-tools">
          {{-- <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

            <div class="input-group-append">
              <button type="submit" class="btn btn-default">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div> --}}
        </div>
      </div>


      <!-- /.card-header -->
      <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap" id="myTable">
          <thead>
            <tr>
              <th>ID</th>
              <th>Book Title</th>
              <th>Author</th>
              <th>Genre</th>
              <th>Image</th>
              <th>Published Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
              @foreach($books as $books)
            <tr>
                <input type="hidden" id="delete_id" name="delete_id" value="{{$books->id}}">
              <td>{{$loop->index+1}}</td>
              <td>{{$books->title}}</td>
              <td>{{$books->author->full_name}}</td>
              <td>{{$books->genre->name}}</td>
              <td><image src="{{asset('uploads/'.$books->image)}}" height="40px" width="60px"></image></td>
              <td>{{$books->published_date}}</td>
              <td>
                  <form action="{{route('book.destroy',$books->id)}}" method="post" >
                    @csrf @method('delete')

                    <a href="{{route('book.edit',$books->id)}}" class="btn btn-warning"><i class="fa fa-pencil-alt"></i></a>
                    <button type="submit" id="delete-btn" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                  </form>


              </td>
            </tr>
            @endforeach

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->

</div>
    <!-- /.card -->
  </div>

  <script>
      $(function(){

          $('#myTable').DataTable();
           $("#delete-btn").on("click",function(e){
            e.preventDefault();
            var delete_id = $("#delete_id").val();

            Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var data = {
                                "_token":$('meta[name="csrf-token"]').attr('content'),
                                'id':delete_id
                            }
                           $.ajax({
                               type:"DELETE",
                               url:"/book/" + delete_id,
                               data:data,
                               success:function(response){
                                  location.reload();
                                   Swal.fire(
                                         'Deleted!',
                                         'Your file has been deleted.',
                                        'success'
                                   )
                               }
                           })

                        }
                    });

          });
      });

  </script>
   {{-- @if (Session::has('msg'))
  <script>
      toastr.success("{{!!Session::get('msg')!!}}");
  </script>
   @endif --}}
@endsection
