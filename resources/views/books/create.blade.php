@extends('layouts.master')
@section('title')
Sale
@endsection
@section('content')
<div class="container">
    @if (session()->has('msg'))
        <div class="alert alert-info" role="alert">
            {{session('msg')}}
          </div>

    @endif

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">ADD BOOKS</h3>
      </div>
      <!-- /.card-header -->


      <!-- form start -->
      <form action="{{route('book.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="title">Book Title</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Enter Book Title" value="{{old('title')}}">
                      </div>
                </div>
            </div>
         <div class="row">
             <div class="col-md-6">

                <div class="form-group">
                    <label for="author">Author</label>
                    <select name="author_id" id="" class="form-control">
                        <option value="" selected disabled>Select Author</option>
                        @foreach($author as $author)
                        <option value="{{$author->id}}">{{$author->full_name}}</option>
                        @endforeach
                    </select>
                    {{-- <input type="text" name="author_name" class="form-control" id="author" placeholder="Enter Book Author" value="{{old('title')}}"> --}}
                  </div>
             </div>
             <div class="col-md-6">
                <div class="form-group">
                    <label for="author">Genre</label>
                    <select name="genre_id" id="" class="form-control">
                        <option value="" selected disabled>Select Genre</option>
                        @foreach($genre as $genre)
                        <option value="{{$genre->id}}">{{$genre->name}}</option>
                        @endforeach
                    </select>
                    {{-- <input type="text" name="author_name" class="form-control" id="author" placeholder="Enter Book Author" value="{{old('title')}}"> --}}
                  </div>

             </div>
         </div>

         <div class="row">
             <div class="col-md-12">
                <div class="form-group">
                    <label for="published_date">Published Date</label>
                    <input type="date" max="@php echo date('Y-m-d') @endphp" name="published_date" class="form-control" id="published_date" value=@php echo date('Y-m-d') @endphp>
                  </div>
             </div>
         </div>

         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                   <label for="image">Image</label>
                   <input type="file"  name="image" class="form-control" id="image"  accept="image/*">
                 </div>

                 {{-- <image src="{{asset('uploads/'.$books->image)}}" style="height:20px, with:20px"></image> --}}
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                   <label for="summary">Summary</label>
                  <textarea name="summary" id="summary" cols="30" rows="10" class="form-control"></textarea>
                 </div>

                 {{-- <image src="{{asset('uploads/'.$books->image)}}" style="height:20px, with:20px"></image> --}}
            </div>
        </div>


        </div>


        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a href="{{route('book.index')}}" class="btn btn-danger">Cancel</a>
        </div>
     </form>
    </div>
    <!-- /.card -->





  </div>

  <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
  <script type="text/javascript">
  CKEDITOR.replace('summary', {
  filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",
  filebrowserUploadMethod: 'form'
  });

@endsection
