@extends('layouts.master')
@section('title')
    Sale
@endsection
@section('content')
    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif <!-- general form elements -->
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">EDIT BOOKS</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('book.update', $books->id) }}" method="post" enctype="multipart/form-data">
                @csrf @method('put')
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="title">Book Title</label>
                                <input type="text" name="title" class="form-control" id="title"
                                    placeholder="Enter Book Title" value="{{ $books->title }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="author">Author</label>
                                <select name="author_id" id="author" class="form-control">
                                    @foreach ($author as $author)
                                        <option value="{{ $author->id }}"
                                            {{ $author->id == $books->author_id ? 'Selected' : '' }}>
                                            {{ $author->full_name }}</option>
                                    @endforeach
                                </select>
                                {{-- <input type="text" name="author_name" class="form-control" id="author" placeholder="Enter Book Author" value="{{$books->author_name}}"> --}}
                            </div>
                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="genre">Genre</label>
                                <select name="genre_id" id="genre" class="form-control">
                                    @foreach ($genre as $genre)
                                        <option value="{{ $genre->id }}"
                                            {{ $genre->id == $books->genre_id ? 'Selected' : '' }}>
                                            {{ $genre->name }}</option>
                                    @endforeach
                                </select>
                                {{-- <input type="text" name="author_name" class="form-control" id="author" placeholder="Enter Book Author" value="{{$books->author_name}}"> --}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="published_date">Published Date</label>
                                <input type="date" name="published_date" class="form-control" id="published_date"
                                   max="@php echo date('Y-m-d') @endphp" value="{{ $books->published_date }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" name="image" accept="image/*" class="form-control" id="image">
                            </div>
                            <img src="{{ asset('uploads/'.$books->image) }}" style="width: 100px" id="one">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                           <div class="form-group">
                               <label for="summary">Summary</label>
                              <textarea name="summary" id="summary" cols="60" rows="10">{{$books->summary}}</textarea>
                             </div>

                             {{-- <image src="{{asset('uploads/'.$books->image)}}" style="height:20px, with:20px"></image> --}}
                        </div>
                    </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ route('book.index') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.card -->





    </div>
@endsection
