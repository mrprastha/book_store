<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Sale;

class SaleController extends Controller
{

    public function index()
    {
        $sale = Sale::latest()->get();
        return view('sale.index',compact('sale'));
    }


    public function create()
    {
        $book = Book::orderBy('title','ASC')->get();
        $sale = Sale::all();
        return view('sale.create',compact('book','sale'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'book_id'=>'required',
            'price'=>'numeric|required',
            'customer_name'=>'required',
            'customer_contact_no'=>'string|required|min:10',
            'sales_date'=>'required',
        ]);

        $sale = new Sale();
        $sale->book_id = $request->book_id;
        $sale->price = $request->price;
        $sale->customer_name = $request->customer_name;
        $sale->customer_contact_no = $request->customer_contact_no;
        $sale->sales_date = $request->sales_date;
        $sale->save();

        $request->session()->flash('msg','Data inserted successfully');

        return redirect()->route('sale.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $book = Book::orderBy('title','ASC')->get();
        $sale = Sale::findOrFail($id);
        return view('sale.edit',compact('book','sale'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'book_id'=>'required',
            'price'=>'numeric|required',
            'customer_name'=>'required',
            'customer_contact_no'=>'string|required|min:10',
            'sales_date'=>'required',
        ]);

        $sale =Sale::findOrFail($id);
        $sale->book_id = $request->book_id;
        $sale->price = $request->price;
        $sale->customer_name = $request->customer_name;
        $sale->customer_contact_no = $request->customer_contact_no;
        $sale->sales_date = $request->sales_date;
        $sale->save();

        $request->session()->flash('msg','Data updated successfully');
        return redirect()->route('sale.index');
    }


    public function destroy(Request $request,$id)
    {
        $sale = new Sale();
        $sale->destroy($id);

        $request->session()->flash('msg','Data deleted successfully');
    }
}
