<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;

class GenreController extends Controller
{

    public function index()
    {
        $genre = Genre::latest()->get();
        return view('genre.index',compact('genre'));
    }


    public function create()
    {
        return view('genre.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
        ]);

        $genre = new Genre();
        $genre->name = $request->name;
        $genre->save();
        $request->session()->flash('msg',"Data inserted successfully");
        return redirect()->route('genre.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $genre = Genre::findOrFail($id);
        return view('genre.edit',compact('genre'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required'
        ]);

        $genre = Genre::findOrFail($id);
        $genre->name = $request->name;
        $genre->save();

        $request->session()->flash('msg',"Genre updated successfully");
        return redirect()->route('genre.index');
    }


    public function destroy(Request $request,$id)
    {
        $genre = new Genre();
        $genre->destroy($id);
        $request->session()->flash('msg',"Genre deleted successfully");

    }
}
