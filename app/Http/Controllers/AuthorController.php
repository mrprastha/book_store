<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{

    public function index()
    {
        $author = Author::latest()->get();
        return view('author.index',compact('author'));
    }


    public function create()
    {
        return view('author.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'full_name'=>'required'
        ]);

        $author = new Author();
        $author->full_name = $request->full_name;
        $author->save();
        $request->session()->flash('msg',"Data inserted successfully");
        return redirect()->route('author.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $author = Author::findOrFail($id);
        return view('author.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'full_name'=>'required'
        ]);

        $author = Author::findOrFail($id);
        $author->full_name = $request->full_name;
        $author->save();

        $request->session()->flash('msg',"Author updated successfully");
        return redirect()->route('author.index');
    }


    public function destroy(Request $request,$id)
    {
        $author = new Author();
        $author->destroy($id);
        $request->session()->flash('msg',"Author deleted successfully");

    }
}
