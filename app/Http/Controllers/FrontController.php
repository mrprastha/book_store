<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use DB;

class FrontController extends Controller
{

    public function index()
    {
        $books = Book::latest()->take(12)->get();
        return view('front.card',compact('books'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $book = Book::findOrFail($id);
        return view('front.show',compact('book'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
