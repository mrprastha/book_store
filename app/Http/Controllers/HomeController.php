<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('home');
    }

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }

    public function display()
    {
        $books = Book::all();
        return view('hh',compact('books'));
    }
}
