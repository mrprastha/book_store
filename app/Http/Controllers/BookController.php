<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BookController extends Controller
{

    public function index()
    {
        $books = Book::latest()->get();
        return view('books.index', compact('books'));
    }

    public function create()
    {
        $author = Author::orderBy('full_name', "ASC")->get();
        $genre = Genre::orderBy('name', 'ASC')->get();
        $books = Book::all();
        return view('books.create', compact('author', 'genre','books'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'author_id' => 'required',
            'genre_id' => 'required',
            'published_date' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg'
        ],
            [
                'title.required' => "Book Title is required",
                'author_id.required' => "Author Name is required",
                'genre_id.required' => "Genre Name is required",
            ]
        );
        $books = new Book();

        $random = Str::random(10);
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $books->image = $filename;
            }
        }

        $books->title = $request->title;
        $books->author_id = $request->author_id;
        $books->summary = $request->summary;
        $books->genre_id = $request->genre_id;
        $books->published_date = $request->published_date;
        $books->save();
        $request->session()->flash('msg', "Data was inserted successfully");
        return redirect()->route('book.index');

    }

    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $books = Book::findOrFail($id);
        $author = Author::orderBy('full_name', "ASC")->get();
        $genre = Genre::orderBy('name', 'ASC')->get();
        return view('books.edit', compact('books', 'author', 'genre'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required',
            'author_id' => 'required',
            'genre_id' => 'required',
            'published_date' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg'

        ],
            [
                'title.required' => "Book Title is required",
                'author_id.required' => "Author Name is required",
                'genre_id.required' => "Genre Name is required",
            ]
        );

        $books = Book::findOrFail($id);

        $random = Str::random(10);
        if ($request->hasFile('image')) {
            $image_tmp = $request->file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random . '.' . $extension;
                $image_path = 'uploads/' . $filename;
                Image::make($image_tmp)->save($image_path);
                $books->image = $filename;
            }
        }


        $books->title = $request->title;
        $books->summary = $request->summary;
        $books->author_id = $request->author_id;
        $books->genre_id = $request->genre_id;
        $books->published_date = $request->published_date;
        $books->save();
        $request->session()->flash('msg', "Data was updated successfully");
        return redirect()->route('book.index');
    }

    public function destroy(Request $request, $id)
    {
        $books = new Book();
        $books->destroy($id);
        $request->session()->flash('msg', "Data was deleted successfully");
        // return redirect()->route('book.index');
    }
}
