<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FrontController;


// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','FrontController@index' );
Route::get('/book/{id}','FrontController@show')->name('frontbook.show');

Route::resource('book','BookController')->middleware('auth');

Route::resource('sale','SaleController');

Route::resource('author','AuthorController');

Route::resource('genre','GenreController');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/logout','HomeController@logout');


Route::post('ckeditor', 'CkeditorFileUploadController@store')->name('ckeditor.upload');
